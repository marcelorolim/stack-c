#ifndef STACK_H_   /* guard */
#define STACK_H_

typedef struct element{
    char string[10];
} element_t;

typedef struct stack{
    int top; // top of stack
    int cap; // capacity
    element_t *content; // elements of the stack
} stack_t;

/* criar pilha de acordo com tamanho passado */
stack_t stck_create(int len);

/* iniciar uma para pilha por referencia */
void stck_init(stack_t *pilha, int len);

int stck_is_empty(stack_t *pilha);
int stck_is_full(stack_t *pilha);
int stck_get_size(stack_t *pilha);

/* 
	add item a pilha 
	retorna 0 (zero) caso não possa ser adicionado
*/
int stck_push(stack_t *pilha, element_t elem);

/* remover item da pilha */
element_t stck_pop(stack_t *pilha);

void stck_destroy(stack_t *pilha);

#endif