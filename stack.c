#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

stack_t stck_create(int len){
    stack_t pilha;
    pilha.content = malloc(sizeof *pilha.content);

    if(!pilha.content) exit(1);
    
    pilha.top = -1;
    pilha.cap = len;
    return pilha;
}

void stck_init(stack_t *pilha, int len){
    pilha->content = malloc(sizeof *pilha->content * len);
    if(!pilha->content){
        fprintf(stderr, "Unable to allocate memory for stack.\n");
        exit(1);
    }
    
    pilha->top = -1;
    pilha->cap = len;
}

void stck_destroy(stack_t *pilha){
    free(pilha->content);

    pilha->top = -1;
    pilha->cap = 0;
}

/* top < 0 */
int stck_is_empty(stack_t *pilha){
    return pilha->top < 0;
}

/* top >= cap */
int stck_is_full(stack_t *pilha){
    return pilha->top >= (pilha->cap - 1);
}

int stck_get_size(stack_t *pilha){
    return pilha->top + 1;
}

int stck_push(stack_t *pilha, element_t elem){

    if(stck_is_full(pilha)) return 0;

    else if(pilha->top == -1){
    	/* a criacao de uma nova stack garante */
        pilha->content[++pilha->top] = elem;
    }else{
        int n;
        pilha->top++;

        pilha->content = realloc(pilha->content,sizeof *pilha->content * (pilha->top + 1) );

        if(!pilha->content){
            fprintf(stderr,"Unable to allocate memory for stack\n");
            exit(1);
        }

        pilha->content[pilha->top] = elem;
    }
    return 1;
}

element_t stck_pop(stack_t *pilha){
    element_t elem = {};

    if(stck_is_empty(pilha)) return elem;


    return pilha->content[pilha->top--];
}